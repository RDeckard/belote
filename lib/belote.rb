# frozen_string_literal: true

require "zeitwerk"

loader = Zeitwerk::Loader.new
loader.push_dir(__dir__)
loader.push_dir("#{__dir__}/app")
loader.push_dir("#{__dir__}/app/models")
loader.push_dir("#{__dir__}/app/services")

loader.ignore("#{__dir__}/test.rb")

case $PROGRAM_NAME
when "bin/run", %r{lib/belote.rb}
  loader.ignore("#{__dir__}/belote.rb")
end

if $PROGRAM_NAME == "bin/console"
  loader.enable_reloading
  define_method(:reload!) { loader.reload }
end

loader.setup

loader.eager_load

case $PROGRAM_NAME
when "bin/run", %r{lib/belote.rb}
  time = Time.now
  # The requests' queue
  QUEUE = Ractor.new do
    loop do
      Ractor.yield(Ractor.receive)
    end
  end

  # Number of running workers
  COUNT = 4

  # Worker ractors
  workers =
    Array.new(COUNT) do |i|
      Ractor.new(QUEUE, name: "ractor#{i + 1}") do |queue|
        loop do
          game = queue.take

          t = Time.now
          Ractor.yield(game.run)
          puts (Time.now - t).to_f
        end
      end
    end

  task_number = 0

  (ARGV.first&.to_i || 100).times do
    Solvers::SolverBase.descendants.each do |team1_solver|
      Solvers::SolverBase.descendants.each do |team2_solver|
        next if team1_solver == team2_solver

        task_number += 1
        QUEUE.send(Game.new(team1_solver, team2_solver))
      end
    end
  end

  aggregated_results =
    task_number.times.each_with_object(Hash.new(0)) do |_, h|
      Ractor.select(*workers).then do |_, results|
        results.each do |result|
          solver_name, score = result
          h[solver_name] += score
        end
      end
    end

  aggregated_results.sort_by { _2 }.reverse_each { |solver_name, score| puts "#{solver_name}: #{score}" }
  puts "TOTAL: #{(Time.now - time).to_f}"
end
