# frozen_string_literal: true

# The requests' queue
QUEUE = Ractor.new do
  loop do
    Ractor.yield(Ractor.receive, move: true)
  end
end

# Number of running workers
COUNT = 4

# Worker ractors
workers =
  Array.new(COUNT) do |i|
    Ractor.new(QUEUE, name: "ractor#{i}") do |queue|
      loop do
        data = queue.take

        10_000_000.times { _1**2 }

        Ractor.yield "Hello #{data} from #{name}!"
      end
    end
  end

16.times { QUEUE.send(_1, move: true) }

16.times { p Ractor.select(*workers) }
