# frozen_string_literal: true

# Cards deck Generator
module Cards
  module DeckGenerator
    extend self # rubocop:disable Style/ModuleFunction

    def call
      Card::COLORS.flat_map do |color|
        Card::VALUE_NAMES.map do |value_name|
          Card.new(color: color, value_name: value_name)
        end
      end
    end

    def count
      @count ||= Card::COLORS.count * Card::VALUE_NAMES.count
    end
  end
end
