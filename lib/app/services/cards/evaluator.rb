# frozen_string_literal: true

module Cards
  module Evaluator
    extend self # rubocop:disable Style/ModuleFunction

    def filter_allowed(cards, stack_of_cards: [], trump_color: nil)
      tenant_card = StackEvaluator.tenant_of(stack_of_cards, trump_color: trump_color)

      cards.select do |card|
        next true unless tenant_card

        if [tenant_card.color, card.color].all? { _1 == trump_color }
          card.owner.same_team?(tenant_card.owner) ||
            card.stronger_than?(tenant_card, trump_color: trump_color) ||
            !Evaluator.can_beat?(cards, tenant_card: tenant_card, trump_color: trump_color)
        else
          card.color == tenant_card.color || cards.none? { _1.color == tenant_card.color }
        end
      end
    end

    def can_beat?(cards, tenant_card:, trump_color: nil)
      cards.any? { |card| card.stronger_than?(tenant_card, trump_color: trump_color) }
    end

    def stronger_than(cards, tenant_card:, trump_color: nil)
      cards.select { |card| card.stronger_than?(tenant_card, trump_color: trump_color) }
    end

    def sort_by_strenght(cards, trump_color: nil)
      cards.sort_by { _1.strenght(trump_color: trump_color) }
    end

    def min_strenght(cards, trump_color: nil)
      cards.min_by { _1.strenght(trump_color: trump_color) }
    end

    def max_strenght(cards, trump_color: nil)
      cards.max_by { _1.strenght(trump_color: trump_color) }
    end
  end
end
