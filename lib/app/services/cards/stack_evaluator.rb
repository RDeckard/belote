# frozen_string_literal: true

module Cards
  module StackEvaluator
    extend self # rubocop:disable Style/ModuleFunction

    def tenant_of(stack_of_cards, trump_color: nil)
      tenant_card = nil

      stack_of_cards.each do |card|
        tenant_card = card if tenant_card.nil? || card.stronger_than?(tenant_card, trump_color: trump_color)
      end

      tenant_card
    end
  end
end
