# frozen_string_literal: true

class Table
  include Utils::Validable

  attr_accessor :stack_of_cards

  validates "stack_of_cards", lambda { |table|
                                table.stack_of_cards.is_a?(Array) && table.stack_of_cards.count <= Game::NB_OF_PLAYERS
                              },
            error_message:    "must be an array of 0 to #{Game::NB_OF_PLAYERS} items"
  validates "stack_of_cards", ->(table) { table.stack_of_cards.all? { _1.is_a?(Card) && _1.valid? } },
            error_message:    "has one or more invalid cards"
  validates "stack_of_cards", ->(table) { table.stack_of_cards.none? { _1.owner.nil? } },
            error_message:    "has one or more cards without `owner`"

  def initialize
    @stack_of_cards = []
  end
end
