# frozen_string_literal: true

class Player
  include Utils::Validable

  DEFAULT_SOLVER = Solvers::Random

  attr_reader   :index
  attr_accessor :hand_cards, :won_cards, :solver

  validates "index",       ->(player) { player.index.is_a?(Integer) && !player.index.negative? },
            error_message: "must be a non-negative integer"

  validates "hand_cards",  ->(player) { player.hand_cards.is_a?(Array) },
            error_message: "must be an array"
  validates "hand_cards",  ->(player) { player.hand_cards.all? { _1.is_a?(Card) && _1.valid? } },
            error_message: "has one or more invalid cards in hand"

  validates "won_cards",    ->(player) { player.won_cards.is_a?(Array) },
            error_message: "must be an array"
  validates "won_cards",    ->(player) { player.won_cards.all? { _1.is_a?(Card) && _1.valid? } },
            error_message: "has one or more invalid won cards"

  validates "solver",      ->(player) { player.solver.respond_to?(:call) },
            error_message: "must have a callable solver"

  def initialize(index, solver: nil)
    @index      = index
    @hand_cards = []
    @won_cards  = []
    @solver     = solver || DEFAULT_SOLVER
  end

  def solve(...)
    @solver.call(self, ...)
  end

  def same_team?(other)
    (index + other.index).even?
  end
end
