# frozen_string_literal: true

class Card
  include Utils::Validable

  COLORS = %w[Spades Hearts Diamonds Clubs].freeze

  DEFAULT_VALUES = {
    "Ace"   => 11,
    "10"    => 10,
    "King"  => 4,
    "Queen" => 3,
    "Jack"  => 2
  }.tap { |h| h.default = 0 }.freeze
  TRUMP_VALUES =
    DEFAULT_VALUES.merge(
      {
        "Jack" => 20,
        "9"    => 14
      }
    ).freeze
  VALUE_NAMES = ((7..9).to_a.map!(&:to_s) + DEFAULT_VALUES.keys.reverse!).map!(&:freeze).freeze

  NAME_FORMAT = "%<value_name>s of %<color>s"

  CODE_BY_COLORS      = COLORS.zip("ABCD".chars).to_h.freeze
  CODE_BY_VALUE_NAMES = VALUE_NAMES.zip("789BDEA1".chars).to_h.freeze

  attr_reader   :value_name, :color
  attr_accessor :owner

  validates "color",       ->(card) { COLORS.include?(card.color) },
            error_message: ->(card) { "'#{card.color}' is not permitted" }

  validates "value_name",  ->(card) { VALUE_NAMES.include?(card.value_name) },
            error_message: ->(card) { "'#{card.value_name}' is not permitted" }

  validates "owner",       ->(card) { card.owner.nil? || card.owner.is_a?(Player) },
            error_message: "must be a player"

  def initialize(color:, value_name:)
    @color      = color
    @value_name = value_name
  end

  def name
    @name ||= format(NAME_FORMAT, value_name: value_name, color: color)
  end

  def icon
    @icon ||= begin
      codepoint = "1F0#{CODE_BY_COLORS[color]}#{CODE_BY_VALUE_NAMES[value_name]}"

      [codepoint.hex].pack("U")
    end
  end

  def icon_and_name
    @icon_and_name ||= "#{icon} #{name}"
  end

  def inspect
    "#<#{self.class.name}: #{name}>"
  end

  def ==(other)
    self.class == other.class && name == other.name
  end

  # Return the card value at the end of a game
  def value(trump_color: nil)
    case color
    when trump_color then TRUMP_VALUES
    else                  DEFAULT_VALUES
    end[value_name]
  end

  # Return a float as "strenght" evaluation (where a '8' beat a '7' of the same color)
  def strenght(trump_color: nil)
    strenght = value(trump_color: trump_color)
    strenght = Float("0.#{value_name}") if strenght.zero?
    strenght += 100 if color == trump_color

    Float(strenght)
  end

  def stronger_than?(tenant_card, trump_color: nil)
    return true if tenant_card.nil?

    if color == tenant_card.color
      match_result = strenght(trump_color: trump_color) <=> tenant_card.strenght(trump_color: trump_color)
      raise "Can't find a winner between '#{inspect}' & '#{tenant_card.inspect}'!" if match_result.zero?

      match_result.positive?
    else
      color == trump_color
    end
  end
end
