# frozen_string_literal: true

module Solvers
  class SoloMin < SolverBase
    def call
      Cards::Evaluator.min_strenght(allowed_cards, trump_color: trump_color)
    end
  end
end
