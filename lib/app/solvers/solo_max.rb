# frozen_string_literal: true

module Solvers
  class SoloMax < SolverBase
    def call
      Cards::Evaluator.max_strenght(allowed_cards, trump_color: trump_color)
    end
  end
end
