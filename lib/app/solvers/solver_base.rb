# frozen_string_literal: true

module Solvers
  class SolverBase
    def self.call(...)
      new(...).call
    end

    attr_reader :player, :allowed_cards, :stack_of_cards, :trump_color

    def self.descendants
      ObjectSpace.each_object(Class).select { _1 < self }
    end

    def initialize(player, allowed_cards:, stack_of_cards:, trump_color:)
      @player          = player
      @allowed_cards   = allowed_cards
      @stack_of_cards  = stack_of_cards
      @trump_color     = trump_color
    end

    def call
      raise "#call method must be implemented in #{self.class.name}."
    end

    def tenant_card
      @tenant_card ||=
        Cards::StackEvaluator.tenant_of(stack_of_cards, trump_color: trump_color)
    end

    def name
      self.class.name
    end
  end
end

# require all descendants solver classes even in case of lazy loading
Dir["#{__dir__}/*"].each { require _1 }
