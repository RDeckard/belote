# frozen_string_literal: true

module Solvers
  class SoloMinMax < SolverBase
    def call
      if Cards::Evaluator.can_beat?(allowed_cards, tenant_card: tenant_card, trump_color: trump_color)
        Cards::Evaluator.max_strenght(allowed_cards, trump_color: trump_color)
      else
        Cards::Evaluator.min_strenght(allowed_cards, trump_color: trump_color)
      end
    end
  end
end
