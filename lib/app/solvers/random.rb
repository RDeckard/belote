# frozen_string_literal: true

module Solvers
  class Random < SolverBase
    def call
      allowed_cards.sample
    end
  end
end
