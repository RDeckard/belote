# frozen_string_literal: true

class Game
  include Utils::Validable

  NB_OF_PLAYERS  = 4
  PLAYER_INDICES = NB_OF_PLAYERS.times.to_a.freeze # [0, 1, 2, 3]

  attr_reader :players, :table, :trump_color, :next_turn_first_player

  validates "players",     ->(game) { game.players.is_a?(Array) && game.players.count == NB_OF_PLAYERS },
            error_message: "must be an array of #{NB_OF_PLAYERS} items"
  validates "players",     ->(game) { game.players.all? { _1.is_a?(Player) && _1.valid? } },
            error_message: "has one or more invalid players"
  validates "players",     ->(game) { game.players.all? { PLAYER_INDICES.include?(_1.index) } },
            error_message: "must all have an index included in #{PLAYER_INDICES}"
  validates "players",     ->(game) { game.players.map { _1.hand_cards.count }.uniq.one? },
            error_message: "must all have the same amount of cards in their hands"

  validates "table",       ->(game) { game.table.is_a?(Table) && game.table.valid? },
            error_message: "must be a valid table"

  validates "trump_color", ->(game) { Card::COLORS.include?(game.trump_color) },
            error_message: ->(game) { "'#{game.trump_color}' is not permitted" }

  validates "next_turn_first_player", ->(game) { game.next_turn_first_player.is_a?(Player) },
            error_message:            "must be a player"

  validates "all cards",
            lambda { |game|
              game.table.stack_of_cards.
                dup.concat(game.players.flat_map(&:hand_cards), game.players.flat_map(&:won_cards)).uniq.count ==
                Cards::DeckGenerator.count
            }, error_message: "consistency error"

  def initialize(team1_solver, team2_solver)
    @team1_solver = team1_solver
    @team2_solver = team2_solver
    @players = PLAYER_INDICES.map { Player.new(_1, solver: _1.even? ? team1_solver : team2_solver) }
    @table   = Table.new

    @next_turn_first_player = @players.first
  end

  def run
    prepare
    play_turns
    compute_score
  end

  def prepare
    distribute_cards
    @trump_color = Card::COLORS.sample
    # validate!
  end

  def play_turns
    @next_turn_first_player.hand_cards.count.times do
      play_turn
      # validate!
    end
  end

  def play_turn
    sorted_turn_players.each do |player|
      allowed_cards =
        Cards::Evaluator.
          filter_allowed(player.hand_cards, stack_of_cards: @table.stack_of_cards, trump_color: @trump_color)

      # puts "-" * 10
      # puts "Trump color: #{@trump_color}", nil
      # puts "#{player.index + 1}. Player's cards: (#{player.hand_cards.count})",
      #      player.hand_cards.map(&:icon_and_name), nil
      # puts "#{player.index + 1}. Permitted cards: (#{allowed_cards.count})", allowed_cards.map(&:icon_and_name), nil

      card_played = call_player_solver(player, allowed_cards)

      player.hand_cards.delete(card_played)
      @table.stack_of_cards << card_played

      # puts "#{player.index + 1}. Table's stack of cards: (#{@table.stack_of_cards.count})",
      #      @table.stack_of_cards.map(&:icon_and_name), nil
    end

    @next_turn_first_player =
      Cards::StackEvaluator.tenant_of(@table.stack_of_cards, trump_color: @trump_color).owner

    @next_turn_first_player.won_cards += @table.stack_of_cards
    @table.stack_of_cards = []
  end

  def compute_score
    [@team1_solver.name, @team2_solver.name].
      zip(@players.partition{ _1.index.even? }).
      to_h do |solver_name, team_players|
        [solver_name, team_players.flat_map(&:won_cards).sum { _1.value(trump_color: @trump_color) }]
      end
  end

  private

  def distribute_cards
    deck_cards = Cards::DeckGenerator.call.shuffle!

    nb_of_cards_per_players, rest = deck_cards.count.divmod(NB_OF_PLAYERS)
    unless rest.zero?
      raise "Can't distribute #{deck_cards.count} cards among #{NB_OF_PLAYERS} players! (remain #{rest})"
    end

    deck_cards.each_slice(nb_of_cards_per_players).with_index do |cards_for_player, player_index|
      player = @players[player_index]

      cards_for_player.each { _1.owner = player }
      player.hand_cards = cards_for_player
    end
  end

  def sorted_turn_players
    (PLAYER_INDICES * 2).                                                   # e.g. [0, 1, 2, 3, 0, 1, 2, 3]
      tap { _1.shift(@next_turn_first_player.index) }.first(NB_OF_PLAYERS). # e.g. [2, 3, 0, 1]
      map { @players[_1] }
  end

  def call_player_solver(player, allowed_cards)
    player.solve(
      allowed_cards:  allowed_cards,
      stack_of_cards: @table.stack_of_cards,
      trump_color:    @trump_color
    ).tap do |choosen_card|
      raise "The player solver must choose a card!" unless choosen_card.is_a?(Card)
    end
  end
end
