# frozen_string_literal: true

module Utils
  module Validable
    def self.included(base)
      base.include InstanceMethods
      base.extend  ClassMethods
    end

    module ClassMethods
      Validation =
        Struct.new(:attribute, :condition, :error_message) do
          define_method(:error_message_call) do |object|
            case error_message
            when Proc
              error_message.(object)
            when String
              error_message
            end
          end
        end

      def validations
        @validations ||= []
      end

      def validates(attribute, condition, error_message:)
        validations << Validation.new(attribute, condition, error_message)
      end

      def instance_name
        @instance_name ||= name.split("::").last.downcase
      end
    end

    module InstanceMethods
      Error =
        Struct.new(:attribute, :message) do
          define_method(:inspect) { "#<#{self.class.name}: #{message}>" }
        end

      def errors
        @errors ||= []
      end

      def valid?
        @errors = []

        self.class.validations.each do |validation|
          next if @errors.any? { |error| error.attribute == validation.attribute } || validation.condition.(self)

          @errors << Error.new(
            validation.attribute,
            "#{validation.attribute} #{validation.error_message_call(self)}"
          )
        end

        @errors.empty?
      end

      def validate!
        return true if valid?

        raise "Invalid #{instance_name}: #{@errors.map(&:message).join(", ")}."
      end

      def instance_name
        self.class.instance_name
      end
    end
  end
end
