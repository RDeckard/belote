# frozen_string_literal: true

require "spec_helper"
require_relative "shared_context_full_deck"

# rubocop:disable Metrics/BlockLength, RSpec/ExampleLength

describe Cards::DeckGenerator do
  describe ".call", :aggregate_failures do
    subject(:deck) { described_class.call }

    include_context "with full deck"

    it "returns an array of the right number of cards" do
      expect(deck).to be_instance_of(Array)
      expect(deck.size).to eq(described_class.count)
      expect(deck.map(&:class).uniq).to eq([Card])
    end

    it "returns all cards" do
      expect(deck).to eq(
        [
          card_7_of_spades,
          card_8_of_spades,
          card_9_of_spades,
          card_jack_of_spades,
          card_queen_of_spades,
          card_king_of_spades,
          card_10_of_spades,
          card_ace_of_spades,
          card_7_of_hearts,
          card_8_of_hearts,
          card_9_of_hearts,
          card_jack_of_hearts,
          card_queen_of_hearts,
          card_king_of_hearts,
          card_10_of_hearts,
          card_ace_of_hearts,
          card_7_of_diamonds,
          card_8_of_diamonds,
          card_9_of_diamonds,
          card_jack_of_diamonds,
          card_queen_of_diamonds,
          card_king_of_diamonds,
          card_10_of_diamonds,
          card_ace_of_diamonds,
          card_7_of_clubs,
          card_8_of_clubs,
          card_9_of_clubs,
          card_jack_of_clubs,
          card_queen_of_clubs,
          card_king_of_clubs,
          card_10_of_clubs,
          card_ace_of_clubs
        ]
      )
    end
  end
end

# rubocop:enable Metrics/BlockLength, RSpec/ExampleLength
