# frozen_string_literal: true

require "spec_helper"
require_relative "shared_context_full_deck"

# rubocop:disable RSpec/ExampleLength

describe Cards::StackEvaluator do
  describe ".tenant_of", :aggregate_failures do
    include_context "with full deck"

    let(:stack_of_card) { [card_10_of_spades, card_9_of_spades, card_7_of_diamonds, card_king_of_hearts] }

    it "works", :aggregate_failures do
      expect(
        described_class.tenant_of(stack_of_card)
      ).to eq(card_10_of_spades)

      expect(
        described_class.tenant_of(stack_of_card, trump_color: "Spades")
      ).to eq(card_9_of_spades)

      expect(
        described_class.tenant_of(stack_of_card, trump_color: "Hearts")
      ).to eq(card_king_of_hearts)

      expect(
        described_class.tenant_of(stack_of_card, trump_color: "Diamonds")
      ).to eq(card_7_of_diamonds)
    end
  end
end

# rubocop:enable RSpec/ExampleLength
