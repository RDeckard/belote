# frozen_string_literal: true

require "spec_helper"
require_relative "shared_context_full_deck"

# rubocop:disable Metrics/BlockLength, RSpec/ExampleLength, RSpec/MultipleMemoizedHelpers, RSpec/NestedGroups

describe Cards::Evaluator do
  let(:shuffled_deck) { Cards::DeckGenerator.call.shuffle }

  include_context "with full deck"

  describe ".filter_allowed" do
    let(:stack_of_cards) { [card_8_of_hearts, card_10_of_diamonds, card_9_of_diamonds] }
    let(:hand_with_first_color) { [card_7_of_spades, card_9_of_hearts, card_queen_of_spades] }
    let(:hand_without_any_right_colors) { [card_7_of_spades, card_9_of_diamonds, card_queen_of_spades] }

    context "without trump_color" do
      it "works", :aggregate_failures do
        expect(
          described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards)
        ).to contain_exactly(card_9_of_hearts)

        expect(
          described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards)
        ).to contain_exactly(*hand_without_any_right_colors)
      end
    end

    context "without the right trump_color" do
      it "works", :aggregate_failures do
        expect(
          described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards,
                                                                trump_color:    "Clubs")
        ).to contain_exactly(card_9_of_hearts)

        expect(
          described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards,
                                                                        trump_color:    "Clubs")
        ).to contain_exactly(*hand_without_any_right_colors)
      end
    end

    context "with trump_color" do
      let(:player1) { Player.new(1) }
      let(:player2) { Player.new(2) }

      context "with the right trump_color" do
        let(:stack_of_cards) { [card_8_of_hearts, card_10_of_clubs, card_9_of_clubs].each { _1.owner = player1 } }

        context "with strong partner" do
          let(:hand_with_trump_color) do
            [card_7_of_spades, card_9_of_hearts, card_queen_of_clubs, card_jack_of_clubs].each { _1.owner = player1 }
          end

          it "works", :aggregate_failures do
            expect(
              described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_7_of_spades, card_9_of_hearts, card_queen_of_spades)

            expect(
              described_class.filter_allowed(hand_with_trump_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_queen_of_clubs, card_jack_of_clubs)

            expect(
              described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards,
                                                                            trump_color:    "Clubs")
            ).to contain_exactly(*hand_without_any_right_colors)
          end
        end

        context "with weak partner" do
          let(:hand_with_trump_color) do
            [card_7_of_spades, card_9_of_hearts, card_queen_of_clubs, card_jack_of_clubs].each { _1.owner = player2 }
          end

          it "works", :aggregate_failures do
            expect(
              described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_7_of_spades, card_9_of_hearts, card_queen_of_spades)

            expect(
              described_class.filter_allowed(hand_with_trump_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_jack_of_clubs)

            expect(
              described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards,
                                                                            trump_color:    "Clubs")
            ).to contain_exactly(*hand_without_any_right_colors)
          end
        end
      end

      context "with the bad trump_color" do
        let(:stack_of_cards) { [card_8_of_hearts, card_10_of_clubs, card_9_of_clubs].each { _1.owner = player1 } }

        context "with strong partner" do
          let(:hand_with_trump_color) do
            [card_7_of_spades, card_9_of_hearts, card_queen_of_clubs, card_jack_of_clubs].each { _1.owner = player1 }
          end

          it "works", :aggregate_failures do
            expect(
              described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Diamonds")
            ).to contain_exactly(card_9_of_hearts)

            expect(
              described_class.filter_allowed(hand_with_trump_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Diamonds")
            ).to contain_exactly(card_9_of_hearts)

            expect(
              described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards,
                                                                            trump_color:    "Diamonds")
            ).to contain_exactly(*hand_without_any_right_colors)
          end
        end

        context "with weak partner" do
          let(:hand_with_trump_color) do
            [card_7_of_spades, card_9_of_hearts, card_queen_of_clubs, card_jack_of_clubs].each { _1.owner = player2 }
          end

          it "works", :aggregate_failures do
            expect(
              described_class.filter_allowed(hand_with_first_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_7_of_spades, card_9_of_hearts, card_queen_of_spades)

            expect(
              described_class.filter_allowed(hand_with_trump_color, stack_of_cards: stack_of_cards,
                                                                    trump_color:    "Clubs")
            ).to contain_exactly(card_jack_of_clubs)

            expect(
              described_class.filter_allowed(hand_without_any_right_colors, stack_of_cards: stack_of_cards,
                                                                            trump_color:    "Clubs")
            ).to contain_exactly(*hand_without_any_right_colors)
          end
        end
      end
    end
  end

  describe ".can_beat?" do
    let(:deck_without_jack_of_diamonds) { shuffled_deck.reject { _1 == card_jack_of_diamonds } }

    context "without trump_color" do
      it "works" do
        expect(
          described_class.can_beat?(deck_without_jack_of_diamonds, tenant_card: card_jack_of_diamonds)
        ).to be true
      end
    end

    context "with same trump_color" do
      it "works" do
        expect(
          described_class.can_beat?(deck_without_jack_of_diamonds, tenant_card: card_jack_of_diamonds,
                                                                   trump_color: "Diamonds")
        ).to be false
      end
    end

    context "with another trump_color" do
      it "works" do
        expect(
          described_class.can_beat?(deck_without_jack_of_diamonds, tenant_card: card_jack_of_diamonds,
                                                                   trump_color: "Spades")
        ).to be true
      end
    end
  end

  describe ".stronger_than", :aggregate_failures do
    let(:deck_without_king_of_diamonds) { shuffled_deck.reject { _1 == card_king_of_diamonds } }

    context "without trump_color" do
      it "works" do
        expect(
          described_class.stronger_than(deck_without_king_of_diamonds, tenant_card: card_king_of_diamonds)
        ).to contain_exactly(
          card_10_of_diamonds, card_ace_of_diamonds
        )
      end
    end

    context "with same trump_color" do
      it "works" do
        expect(
          described_class.stronger_than(deck_without_king_of_diamonds, tenant_card: card_king_of_diamonds,
                                                                       trump_color: "Diamonds")
        ).to contain_exactly(
          card_10_of_diamonds, card_ace_of_diamonds, card_9_of_diamonds, card_jack_of_diamonds
        )
      end
    end

    context "with another trump_color" do
      it "works" do
        expect(
          described_class.stronger_than(deck_without_king_of_diamonds, tenant_card: card_king_of_diamonds,
                                                                       trump_color: "Spades")
        ).to contain_exactly(
          card_10_of_diamonds, card_ace_of_diamonds,
          card_7_of_spades, card_8_of_spades, card_9_of_spades, card_jack_of_spades,
          card_queen_of_spades, card_king_of_spades, card_10_of_spades, card_ace_of_spades
        )
      end
    end
  end

  describe ".sort_by_strenght" do
    let(:sorted_deck) { Cards::DeckGenerator.call }

    context "without trump_color" do
      it "works" do
        expect(
          described_class.sort_by_strenght(shuffled_deck).map(&:strenght)
        ).to eq(
          [0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 2.0, 2.0, 2.0, 2.0,
           3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 10.0, 10.0, 10.0, 10.0, 11.0, 11.0, 11.0, 11.0]
        )
      end
    end

    context "with trump_color" do
      it "works" do
        expect(
          described_class.sort_by_strenght(shuffled_deck, trump_color: "Diamonds").
            map { _1.strenght(trump_color: "Diamonds") }
        ).to eq(
          [0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0,
           4.0, 4.0, 4.0, 10.0, 10.0, 10.0, 11.0, 11.0, 11.0, 100.7, 100.8, 103.0, 104.0, 110.0, 111.0, 114.0, 120.0]
        )
      end
    end
  end

  describe ".min_strenght" do
    context "without trump_color" do
      it "works" do
        expect(
          [card_7_of_spades, card_7_of_hearts, card_7_of_diamonds, card_7_of_clubs]
        ).to include(described_class.min_strenght(shuffled_deck))
      end
    end

    context "with trump_color" do
      it "works" do
        expect(
          [card_7_of_spades, card_7_of_hearts, card_7_of_diamonds, card_7_of_clubs]
        ).to include(described_class.min_strenght(shuffled_deck, trump_color: "Diamonds"))
      end
    end
  end

  describe ".max_strenght" do
    context "without trump_color" do
      it "works" do
        expect(
          [card_ace_of_spades, card_ace_of_hearts, card_ace_of_diamonds, card_ace_of_clubs]
        ).to include(described_class.max_strenght(shuffled_deck))
      end
    end

    context "with trump_color" do
      it "works" do
        expect(described_class.max_strenght(shuffled_deck, trump_color: "Diamonds")).to eq(card_jack_of_diamonds)
      end
    end
  end
end

# rubocop:enable Metrics/BlockLength, RSpec/ExampleLength, RSpec/MultipleMemoizedHelpers, RSpec/NestedGroups
