# frozen_string_literal: true

require "spec_helper"

# rubocop:disable RSpec/MultipleMemoizedHelpers, Metrics/BlockLength

RSpec.shared_context "with full deck" do
  let(:card_7_of_spades)       { Card.new(color: "Spades",   value_name: "7")     }
  let(:card_8_of_spades)       { Card.new(color: "Spades",   value_name: "8")     }
  let(:card_9_of_spades)       { Card.new(color: "Spades",   value_name: "9")     }
  let(:card_jack_of_spades)    { Card.new(color: "Spades",   value_name: "Jack")  }
  let(:card_queen_of_spades)   { Card.new(color: "Spades",   value_name: "Queen") }
  let(:card_king_of_spades)    { Card.new(color: "Spades",   value_name: "King")  }
  let(:card_10_of_spades)      { Card.new(color: "Spades",   value_name: "10")    }
  let(:card_ace_of_spades)     { Card.new(color: "Spades",   value_name: "Ace")   }
  let(:card_7_of_hearts)       { Card.new(color: "Hearts",   value_name: "7")     }
  let(:card_8_of_hearts)       { Card.new(color: "Hearts",   value_name: "8")     }
  let(:card_9_of_hearts)       { Card.new(color: "Hearts",   value_name: "9")     }
  let(:card_jack_of_hearts)    { Card.new(color: "Hearts",   value_name: "Jack")  }
  let(:card_queen_of_hearts)   { Card.new(color: "Hearts",   value_name: "Queen") }
  let(:card_king_of_hearts)    { Card.new(color: "Hearts",   value_name: "King")  }
  let(:card_10_of_hearts)      { Card.new(color: "Hearts",   value_name: "10")    }
  let(:card_ace_of_hearts)     { Card.new(color: "Hearts",   value_name: "Ace")   }
  let(:card_7_of_diamonds)     { Card.new(color: "Diamonds", value_name: "7")     }
  let(:card_8_of_diamonds)     { Card.new(color: "Diamonds", value_name: "8")     }
  let(:card_9_of_diamonds)     { Card.new(color: "Diamonds", value_name: "9")     }
  let(:card_jack_of_diamonds)  { Card.new(color: "Diamonds", value_name: "Jack")  }
  let(:card_queen_of_diamonds) { Card.new(color: "Diamonds", value_name: "Queen") }
  let(:card_king_of_diamonds)  { Card.new(color: "Diamonds", value_name: "King")  }
  let(:card_10_of_diamonds)    { Card.new(color: "Diamonds", value_name: "10")    }
  let(:card_ace_of_diamonds)   { Card.new(color: "Diamonds", value_name: "Ace")   }
  let(:card_7_of_clubs)        { Card.new(color: "Clubs",    value_name: "7")     }
  let(:card_8_of_clubs)        { Card.new(color: "Clubs",    value_name: "8")     }
  let(:card_9_of_clubs)        { Card.new(color: "Clubs",    value_name: "9")     }
  let(:card_jack_of_clubs)     { Card.new(color: "Clubs",    value_name: "Jack")  }
  let(:card_queen_of_clubs)    { Card.new(color: "Clubs",    value_name: "Queen") }
  let(:card_king_of_clubs)     { Card.new(color: "Clubs",    value_name: "King")  }
  let(:card_10_of_clubs)       { Card.new(color: "Clubs",    value_name: "10")    }
  let(:card_ace_of_clubs)      { Card.new(color: "Clubs",    value_name: "Ace")   }
end

# rubocop:enable RSpec/MultipleMemoizedHelpers, Metrics/BlockLength
